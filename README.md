# 

项目使用了 Lombok， 用来减少 java 代码编写 get/set 方法。

Idea 可以安装lombok对应版本的plugin避免get/set方法的报错


http://127.0.0.1:8080/swagger-ui.html
http://127.0.0.1:8080//v2/api-docs

docker run --rm -w /src -it -v /Users/simon/springboot-template.git:/src maven:3.5.4-jdk-8-alpine bash

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:simon.meng/multi-mybatis-demo.git
git push -u origin --all
git push -u origin --tags

mvn clean package -s settings.xml
mvn dependency:resolve -Dclassifier=sources -s settings.xml
mvn com.spotify:dockerfile-maven-plugin:1.4.10:build  -s settings.xml 

curl -ik --cert client.crt --key clientprivate.key "https://localhost:8443/foo/"

mvn mybatis-generator:generate

https://blog.codecentric.de/en/2018/08/x-509-client-certificates-with-spring-security/