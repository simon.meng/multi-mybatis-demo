package com.yoyosys.mt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
//@AutoConfigureMockMvc(secure = true)
public class DemoControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;


    //@Test
    public void shouldReturnDifferentDatabases() throws Exception {
        //@formatter:off
        mockMvc
                .perform(get("/info?id=0001"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{'Mapper':'Database version','anotherMapper':'hsqldbtestdb 2.4.1','oneMapper':'H2TESTDB 1.4.197'}"));
//        mockMvc
//                .perform(get("/info?id=0002"))
//                .andExpect(status().is2xxSuccessful())
//                .andExpect(content().json("{'Mapper':'Database version','anotherMapper':'hsqldbtestdb 2.4.1','oneMapper':'H2TESTDB 1.4.197'}"));
//        mockMvc
//                .perform(get("/info?id=0002"))
//                .andExpect(status().is2xxSuccessful())
//                .andExpect(content().json("{'Mapper':'Database version','anotherMapper':'hsqldbtestdb 2.4.1','oneMapper':'H2TESTDB 1.4.197'}"));

        //@formatter:on
    }

}
