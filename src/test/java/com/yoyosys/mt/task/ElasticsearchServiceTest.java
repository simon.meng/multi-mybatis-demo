package com.yoyosys.mt.task;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

//import org.elasticsearch.script.mustache.SearchTemplateRequestBuilder;

/**
 * Created by simon on 2018/10/25.
 */

public class ElasticsearchServiceTest {
    //@Test
    public void testAddDocument() {
        int i = 0;
        try (RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")))) {
            Reader in = new FileReader("/Users/simon/Desktop/address/poi_db.csv");
            Iterable<CSVRecord> records = CSVFormat.newFormat('|').parse(in);
            BulkRequest request = new BulkRequest();

            for (CSVRecord record : records) {
                Map<String, String> data = new HashMap<>();
                i++;
                data.put("id", i + "");
                data.put("a", record.get(0));
                data.put("b", record.get(1));
                data.put("c", record.get(2));
                data.put("d", record.get(3));
                data.put("e", record.get(4));
                data.put("f", record.get(5));
                request.add(new IndexRequest("twitter", "_doc", i + "").source(data));
                if (i % 100 == 0) {
                    try {
                        BulkResponse bulkResponse = client.bulk(request);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    request = new BulkRequest();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}