package com.yoyosys.mt.task;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 2018/11/27.
 */
public class AddressMaxWordsTest {

    //@Test
    public void testMaxWords() throws IOException {
        Map<Character, Integer> counter = new HashMap();
        LineIterator lineIterator = FileUtils.lineIterator(new File("/Users/simon/Desktop/address/poi_db.csv"), "utf-8");
        while (lineIterator.hasNext()) {
            String[] strs = lineIterator.nextLine().split("\\|");
            if (strs.length < 5) {
                continue;
            }
            char[] charts = strs[4].toCharArray();
            for (char c : charts) {
                counter.put(c, counter.getOrDefault(c, 0) + 1);
            }
        }

        List<Map.Entry<Character, Integer>> list = new ArrayList<>();
        list.addAll(counter.entrySet());
        list.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));

        for (int i = 0; i < 100; i++) {
            System.out.println(list.get(i).getKey() + " -- > " + list.get(i).getValue());
        }

    }
}
