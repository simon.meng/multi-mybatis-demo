package com.yoyosys.mt.task;

import com.yoyosys.mt.domain.DatabaseSettings;
import com.yoyosys.mt.domain.ElasticsearchSettings;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.task.consumer.AbstractConsumer;
import com.yoyosys.mt.task.consumer.ElasticsearchConsumer;
import com.yoyosys.mt.task.exception.TaskFailedException;
import com.yoyosys.mt.task.producer.AbstractProducer;
import com.yoyosys.mt.task.producer.DatabaseProducer;
import org.junit.Test;

/**
 * Created by simon on 2019/2/27.
 */
public class TaskServiceTest {


    //@Test
    public void test() throws InterruptedException {
        TaskService taskService = new TaskServiceImpl();

        TaskItem taskItem = new TaskItem();
        taskItem.setProducerCount(1);
        taskItem.setProducerType(DatabaseProducer.class.getName());
        taskItem.setConsumerCount(4);
        taskItem.setConsumerType(ElasticsearchConsumer.class.getName());


        DatabaseSettings settings = new DatabaseSettings();
        settings.setUsername("postgres");
        settings.setPassword("postgres");
        settings.setJdbcUrl("jdbc:postgresql://localhost:32768/postgres");
        settings.setSql("select * from address limit 30000");
        taskItem.setProducerSettings(settings);

        ElasticsearchSettings elasticsearchSettings = new ElasticsearchSettings();
        elasticsearchSettings.setUrl("127.0.0.1:9200");
        elasticsearchSettings.setIndexName("job17");
        elasticsearchSettings.setDocType("_doc");
        taskItem.setConsumerSettings(elasticsearchSettings);

        taskService.executeTask(taskItem);

    }


    @Test
    public void testAsyncExecute() throws InterruptedException, TaskFailedException {
        TaskItem taskItemA = new TaskItem();
        taskItemA.setProducerType(AProducer.class.getName());
        taskItemA.setConsumerType(Aconsumer.class.getName());

        TaskItem taskItemB = new TaskItem();
        taskItemB.setProducerType(BProducer.class.getName());
        taskItemB.setConsumerType(Bconsumer.class.getName());


        TaskItem taskItemC = new TaskItem();
        taskItemC.setProducerType(CProducer.class.getName());
        taskItemC.setConsumerType(Cconsumer.class.getName());

        TaskItemChain taskItemChain = new TaskItemChain();
        taskItemChain.addTaskitem(taskItemA);
        taskItemChain.addTaskitem(taskItemB);
        taskItemChain.addTaskitem(taskItemC);

        TaskService taskService = new TaskServiceImpl();
        taskService.asyncExecuteTask(taskItemA);

        //Test only
        taskItemC.waitForComplete();
    }

    public static class AProducer extends AbstractProducer<String> {
        public AProducer(TaskItem taskItem) {
            super(taskItem);
        }

        @Override
        protected int produce() {
            for (int i = 0; i < 1000; i++) {
                try {
                    addProduceToQueue("A" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TaskNotRunningException e) {
                    e.printStackTrace();
                    return 1;
                }
            }
            return 0;
        }

        @Override
        protected void onCompleted() {
        }
    }

    public static class Aconsumer extends AbstractConsumer<String> {

        public Aconsumer(TaskItem taskItem) {
            super(taskItem);
        }

        @Override
        protected void consume(String obj) {
            System.out.println(Thread.currentThread().getName() + "A-consumer--->" + obj);
        }

        @Override
        protected void onCompleted() {
        }
    }


    public static class BProducer extends AbstractProducer<String> {
        public BProducer(TaskItem taskItem) {
            super(taskItem);
        }

        @Override
        protected int produce() {
            for (int i = 0; i < 1000; i++) {
                try {
                    addProduceToQueue("B" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TaskNotRunningException e) {
                    e.printStackTrace();
                    return 1;
                }
            }
            return 0;
        }

        @Override
        protected void onCompleted() {
        }
    }

    public static class Bconsumer extends AbstractConsumer<String> {

        public Bconsumer(TaskItem taskItem) {
            super(taskItem);
        }

        @Override
        protected void consume(String obj) {
            System.out.println("B-consumer--->" + obj);
        }

        @Override
        protected void onCompleted() {
        }
    }

    public static class CProducer extends AbstractProducer<String> {
        public CProducer(TaskItem taskItem) {
            super(taskItem);
        }

        @Override
        protected int produce() {
            for (int i = 0; i < 1000; i++) {
                try {
                    addProduceToQueue("C" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (TaskNotRunningException e) {
                    e.printStackTrace();
                    return 1;
                }
            }
            return 0;
        }

        @Override
        protected void onCompleted() {
        }
    }

    public static class Cconsumer extends AbstractConsumer<String> {

        public Cconsumer(TaskItem taskItem) {
            super(taskItem);
        }

        @Override
        protected void consume(String obj) {
            System.out.println("c-consumer--->" + obj);
        }

        @Override
        protected void onCompleted() {
        }
    }

}
