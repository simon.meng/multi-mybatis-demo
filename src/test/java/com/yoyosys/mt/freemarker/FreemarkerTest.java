package com.yoyosys.mt.freemarker;

import com.yoyosys.mt.config.PrimaryDatasourceConfig;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by simon on 2018/10/13.
 */
public class FreemarkerTest {

    //@Test
    public void test() throws Exception {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_27);

        // Specify the source where the template files come from. Here I set a
        // plain directory for it, but non-file-system sources are possible too:
        System.out.println(PrimaryDatasourceConfig.class.getClassLoader().getResource("."));
        cfg.setDirectoryForTemplateLoading(new File("/Users/simon/multi-mybatis-demo.git/src/main/resources/templates"));

        // Set the preferred charset template files are stored in. UTF-8 is
        // a good choice in most applications:
        cfg.setDefaultEncoding("UTF-8");

        // Sets how errors will appear.
        // During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        // Don't log exceptions inside FreeMarker that it will thrown at you anyway:
        cfg.setLogTemplateExceptions(false);

        // Wrap unchecked exceptions thrown during template processing into TemplateException-s.
        cfg.setWrapUncheckedExceptions(true);

        Map<String, Object> root = new HashMap<>();

        // Put string "user" into the root
        root.put("name", "Big Joe");

        Template temp = cfg.getTemplate("hello.ftl");

        Writer out = new OutputStreamWriter(System.out);

        temp.process(root, out);
    }
}
