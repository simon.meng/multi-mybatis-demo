package com.yoyosys.mt.rule;

import com.vip.vjtools.vjkit.logging.PerformanceUtil;
import com.vip.vjtools.vjkit.mapper.XmlMapper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 2019/3/7.
 */
public class RuleTest {

    public void testToXml() {
        Rule sc = new Rule();
        List<Action> list = new ArrayList<>();
        list.add(new Filter(""));
        list.add(new Filter(""));
        list.add(new Replacer("", ""));
        list.add(new Converter());
        list.add(new Script("abc"));
        sc.setActions(list);
        System.out.println(XmlMapper.toXml(sc));
    }

    @Test
    public void testFromXml() throws Exception {
        PerformanceUtil.start();
        Rule rule = XmlMapper.fromXml(IOUtils.resourceToString("/rule.xml", Charset.defaultCharset()), Rule.class);
        for (int i = 0; i <1000; i++) {
            String result = rule.normalize("中国");

        }
        System.out.println(PerformanceUtil.end());

    }
}
