package com.yoyosys.mt;

import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by simon on 2019/1/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class JdbcTemplateTest {

    @Autowired
    @Qualifier("pgJdbcTemplate")
    private JdbcTemplate jdbcTemplate1;


    @Test
    public void test() {

        try (RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200, "http")))) {
            jdbcTemplate1.query("select * From address LIMIT 3", new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet resultSet) throws SQLException {
                    Map<String, Object> data = new HashMap<>();
                    ResultSetMetaData r = resultSet.getMetaData();
                    for (int i = 1; i <= r.getColumnCount(); i++) {
                        System.out.println(r.getColumnName(i));
                        data.put(r.getColumnName(i), resultSet.getObject(i));
                    }
                    data.put("create_time", new Date());
                    data.put("age", 1);
                    data.put("age1", 1.1f);
                    data.put("age2", 1.1666d);
                    data.put("age3", 1.1666d);
                    data.put("age4", 1.1666d);

                    try {
                        IndexRequest indexRequest = new IndexRequest("twitter", "_doc", UUID.randomUUID().toString()).source(data);
                        client.index(indexRequest, RequestOptions.DEFAULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
