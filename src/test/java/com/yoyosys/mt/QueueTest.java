package com.yoyosys.mt;

import org.jctools.queues.SpscArrayQueue;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by simon on 2019/2/24.
 * https://www.baeldung.com/java-concurrency-jc-tools
 */
public class QueueTest {


    @Test
    public void testSpscQueue() throws InterruptedException {
        SpscArrayQueue<Integer> queue = new SpscArrayQueue<>(2);

        Thread producer1 = new Thread(() -> queue.offer(1));
        producer1.start();
        producer1.join();

        Thread producer2 = new Thread(() -> queue.offer(2));
        producer2.start();
        producer2.join();

        Set<Integer> fromQueue = new HashSet<>();
        Thread consumer = new Thread(() -> queue.drain(fromQueue::add));
        consumer.start();
        consumer.join();

        assertThat(fromQueue).containsOnly(1, 2);
    }
}
