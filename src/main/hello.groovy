import groovy.transform.TypeChecked
import groovy.transform.CompileStatic

@TypeChecked
@CompileStatic

void method() {
    def name = "  Guillaume  "

    // String type inferred (even inside GString)
    println "NAME = ${name.toUpperCase()}"
    // Groovy GDK method support
    // (GDK operator overloading too)
    println name.trim()

    int[] numbers = [1, 2, 3]
    // Element n is an int
    for (int n in numbers) {
        println n
    }
}

method()
