package com.yoyosys.mt.web;

/**
 * Created by simon on 2019/2/20.
 */
public class HttpExceptions {

    public static Http400Exception http400Exception(String code, String msg) {
        return new Http400Exception(code, msg);
    }

    public static Http401Exception http401Exception(String code, String msg) {
        return new Http401Exception(code, msg);
    }

    public static Http403Exception http403Exception(String code, String msg) {
        return new Http403Exception(code, msg);
    }

    public static Http404Exception http404Exception(String code, String msg) {
        return new Http404Exception(code, msg);
    }

    public static Http500Exception http500Exception(String code, String msg) {
        return new Http500Exception(code, msg);
    }

    //------------------------------------------------------

    static class HttpException extends RuntimeException {
        public String code;

        public HttpException(String code, String message) {
            super(message);
            this.code = code;
        }
    }

    static class Http400Exception extends HttpException {
        public Http400Exception(String code, String message) {
            super(code, message);
        }
    }

    static class Http401Exception extends HttpException {
        public Http401Exception(String code, String message) {
            super(code, message);
        }
    }

    static class Http403Exception extends HttpException {
        public Http403Exception(String code, String message) {
            super(code, message);
        }
    }

    static class Http404Exception extends HttpException {
        public Http404Exception(String code, String message) {
            super(code, message);
        }
    }

    static class Http500Exception extends HttpException {
        public Http500Exception(String code, String message) {
            super(code, message);
        }
    }
}
