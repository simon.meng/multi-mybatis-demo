package com.yoyosys.mt.web;

/**
 * Created by simon on 2019/2/25.
 */
public class WebHelper {

    public static JsonObject successMessage() {
        return new JsonObject("OK", "success");
    }

    public static JsonObject successMessage(String message) {
        return new JsonObject("OK", message);
    }

    public static JsonObject successMessage(String code, String message) {
        return new JsonObject(code, message);
    }

    public static JsonObject errorMessage(String message) {
        return new JsonObject("ERROR", message);
    }

    public static JsonObject errorMessage(String code, String message) {
        return new JsonObject(code, message);
    }

    public static JsonObject toJsonObject(Object data) {
        return new JsonObject("OK", "success", data);
    }

    public static JsonObject toJsonObject(String message, Object data) {
        return new JsonObject("OK", message, data);
    }

    public static JsonObject toJsonObject(String code, String message, Object data) {
        return new JsonObject(code, message, data);
    }

}
