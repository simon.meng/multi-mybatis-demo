package com.yoyosys.mt.web;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by simon on 2019/2/22.
 */
public class WebContextHolder {

    private static ThreadLocal<Map<String, Object>> threadLocal = ThreadLocal.withInitial(() -> new HashMap());

    public static Map<String, Object> getData() {
        return threadLocal.get();
    }
}
