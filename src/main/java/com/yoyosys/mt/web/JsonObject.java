package com.yoyosys.mt.web;

import java.io.Serializable;

/**
 * Created by simon on 2019/2/25.
 */
public class JsonObject implements Serializable {

    private String requestId = (String) WebContextHolder.getData().get(Constants.HTTP_REQUEST_ID);
    private String code;
    private String message;
    private Object data;


    public JsonObject() {

    }

    public JsonObject(Object data) {
        this.data = data;
    }

    public JsonObject(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public JsonObject(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
