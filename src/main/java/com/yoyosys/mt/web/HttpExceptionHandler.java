package com.yoyosys.mt.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vip.vjtools.vjkit.mapper.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by simon on 2019/2/26
 * 统一异常处理
 */
@RestControllerAdvice
public class HttpExceptionHandler {

    private final static Logger logger = LoggerFactory.getLogger(HttpExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(value = HttpExceptions.Http400Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonObject handleBadRequest(HttpExceptions.Http400Exception e) {
        JsonObject errorResult = WebHelper.errorMessage(e.code, e.getMessage());
        logger.error(JsonMapper.defaultMapper().toJson(errorResult), e);
        return errorResult;
    }


    @ResponseBody
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonObject handleBadRequest(HttpMessageNotReadableException e) {
        String msg;
        String code;
        if (e.getCause() instanceof JsonProcessingException) {
            msg = "Your Json format is invalid, " + e.getMessage();
            code = "BAD_REQUEST_INVALID_JSON_FORMAT";
        } else {
            msg = e.getMessage();
            code = "BAD_REQUEST";
        }
        JsonObject errorResult = WebHelper.errorMessage(code, msg);
        logger.error(JsonMapper.defaultMapper().toJson(errorResult), e);
        return errorResult;
    }


    @ResponseBody
    @ExceptionHandler(value = HttpExceptions.Http401Exception.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public JsonObject handleUnAuthorizedReqeust(HttpExceptions.Http401Exception e) {
        JsonObject errorResult = WebHelper.errorMessage(e.code, e.getMessage());
        logger.error(JsonMapper.defaultMapper().toJson(errorResult), e);
        return errorResult;
    }

    @ResponseBody
    @ExceptionHandler(value = HttpExceptions.Http403Exception.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public JsonObject handleForbiddenRequest(HttpExceptions.Http403Exception e) {
        JsonObject errorResult = WebHelper.errorMessage(e.code, e.getMessage());
        logger.error(JsonMapper.defaultMapper().toJson(errorResult), e);
        return errorResult;
    }


    @ResponseBody
    @ExceptionHandler(value = HttpExceptions.Http404Exception.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonObject handleNotFoundRequest(HttpExceptions.Http404Exception e) {
        JsonObject errorResult = WebHelper.errorMessage(e.code, e.getMessage());
        logger.error(JsonMapper.defaultMapper().toJson(errorResult), e);
        return errorResult;
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public JsonObject handleInternalServerError(Exception e) {
        JsonObject errorResult = WebHelper.errorMessage("INTERNAL_SERVER_ERROR", e.getMessage());
        logger.error(JsonMapper.defaultMapper().toJson(errorResult), e);
        return errorResult;
    }
}
