package com.yoyosys.mt.handler;

import com.vip.vjtools.vjkit.mapper.JsonMapper;
import com.yoyosys.mt.domain.DatabaseSettings;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.postgresql.util.PGobject;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(Object.class)
public class JSONBHandler extends BaseTypeHandler<Object> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
        System.out.println(configuration);
        PGobject pGobject = new PGobject();
        pGobject.setType("jsonb");
        pGobject.setValue(JsonMapper.defaultMapper().toJson(parameter));
        ps.setObject(i, pGobject);
    }

    @Override
    public Object getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        System.out.println(configuration);
        JsonMapper.defaultMapper().fromJson(rs.getString(columnIndex), Object.class);
        return JsonMapper.defaultMapper().fromJson(rs.getString(columnIndex), Object.class);
    }

    @Override
    public Object getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        //return cs.getString(columnIndex);
        System.out.println(configuration);
        return JsonMapper.defaultMapper().fromJson(cs.getString(columnIndex), Object.class);

    }

    @Override
    public Object getNullableResult(ResultSet rs, String columnName) throws SQLException {
        //return rs.getString(columnName);
        System.out.println(configuration);
        return JsonMapper.defaultMapper().fromJson(rs.getString(columnName), Object.class);

    }

    public static void main(String[] args) {
        System.out.println(JsonMapper.defaultMapper().toJson("{}"));
    }

}