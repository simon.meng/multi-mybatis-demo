package com.yoyosys.mt.config;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.inject.Named;
import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.yoyosys.mt.mapper.system", sqlSessionFactoryRef = "systemSqlSessionFactory")
public class SystemDatasourceConfig {

    public static final String MAPPER_LOCATION = "classpath:com/yoyosys/mt/mapper/system/**/*.xml";
    public static final String SYSTEM_DATASOURCE = "system-datasource";

    @Bean(name = SYSTEM_DATASOURCE)
    @ConfigurationProperties("datasources.postgres")
    public HikariDataSource masterDataSource() {
        return new HikariDataSource();
    }

    @Bean(name = "systemTransactionManager")
    public DataSourceTransactionManager systemTransactionManager(@Named(SYSTEM_DATASOURCE) DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "systemSqlSessionFactory")
    public SqlSessionFactory systemSqlSessionFactory(@Named(SYSTEM_DATASOURCE) DataSource systemDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(systemDataSource);
        sessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(SystemDatasourceConfig.MAPPER_LOCATION));

        return sessionFactoryBean.getObject();
    }

    @Bean(name = "systemJdbcTemplate")
    public JdbcTemplate defaultJdbcTemplate(@Named(SYSTEM_DATASOURCE) DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}