package com.yoyosys.mt.config;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.inject.Named;
import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.yoyosys.mt.mapper.primary", sqlSessionFactoryRef = "primarySqlSessionFactory")
public class PrimaryDatasourceConfig {

    public static final String MAPPER_LOCATION = "classpath:com/yoyosys/mt/mapper/system/**/*.xml";
    public static final String PRIMARY_DATASOURCE = "OneDS";

    @Primary
    @Bean(name = PRIMARY_DATASOURCE)
    @ConfigurationProperties("datasources.h2")
    public HikariDataSource primaryDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        return dataSource;
    }

    @Bean(name = "primaryTransactionManager")
    @Primary
    public DataSourceTransactionManager primaryTransactionManager(@Named(PRIMARY_DATASOURCE) DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "primarySqlSessionFactory")
    @Primary
    public SqlSessionFactory primarySqlSessionFactory(@Named(PRIMARY_DATASOURCE) DataSource primaryDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(primaryDataSource);
        sessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(PrimaryDatasourceConfig.MAPPER_LOCATION));

        return sessionFactoryBean.getObject();
    }

    @Bean
    @Primary
    public JdbcTemplate defaultJdbcTemplate(@Named(PRIMARY_DATASOURCE) DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


}