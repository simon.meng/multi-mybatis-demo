package com.yoyosys.mt.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yoyosys.mt.SpringContextUtils;
import com.yoyosys.mt.manager.TaskItemManager;
import com.yoyosys.mt.task.*;
import com.yoyosys.mt.utils.TaskHistoryHelper;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import static com.yoyosys.mt.task.TaskItemStatus.STOPPED_STATUS;

/**
 * Created by simon on 2018/10/25.
 */
@Data
public class TaskItem<E> implements Serializable {

    private String id;
    private String name;
    private String lockStatus;
    private String lockId;
    private int status;

    private Date firstExecutionTime;
    private Date lastExecutionTime;
    private Date startTime;
    private Date endTime;

    //private boolean producing;

    //
    private String producerId;
    private String producerType;
    private int producerCount = 1;
    private int producerCompletedCount;
    private Object producerSettings;
    private int producerStatus;

    //
    private String consumerId;
    private String consumerType;
    private int consumerCount = 4;
    private int consumerCompletedCount;
    private int consumerStatus;
    private Object consumerSettings;

    private Date createTime;
    private Date updateTime;

    private String producerDbSql;
    private String consumerEsIndexName;
    private String consumerEsIdKey;

    /**************************************************************************************/
    @JsonIgnore
    private TaskEventListener taskEventListener;

    @JsonIgnore
    private Map<String, Object> taskContext;

    @JsonIgnore
    private transient LinkedBlockingQueue<E> queue = new LinkedBlockingQueue<>(100);

    @JsonIgnore
    private TaskItemChain taskItemChain;

    public synchronized void increaseProducerCompletedCount() {
        producerCompletedCount++;
        if (producerCompletedCount == producerCount) {
            TaskHistoryHelper.addTaskHistory(this, "producer-completed", "生产完成");
            if (this.getStatus() == TaskItemStatus.RUNNING) {
                this.setStatus(TaskItemStatus.PRODUCE_COMPLETED);
            }
        }
    }

    public synchronized void increaseConsumerCompletedCount() {
        consumerCompletedCount++;

        TaskHistoryHelper.addTaskHistory(this, "consumer completed", "消费者完成: " + consumerCompletedCount + "/" + getConsumerCount() );

        if (consumerCompletedCount == consumerCount) {
            TaskItemManager taskItemManager = SpringContextUtils.getBean(TaskItemManager.class);

            //若正常结束(上一个状态是生产正常结束状态)
            boolean doNextTaskitem = false;
            if (getStatus() == TaskItemStatus.PRODUCE_COMPLETED) {
                TaskHistoryHelper.addTaskHistory(this, "task-completed", "任务完成");
                doNextTaskitem = true;
            }
            setStatus(TaskItemStatus.END);

            if (taskItemManager != null) {
                this.setLastExecutionTime(new Date());
                taskItemManager.update(this);
            }
            if (doNextTaskitem) {
                executeNextTaskitem();
            }
        }
    }

    /**
     *  当存在链式的任务时,执行一下个任务
     */
    private void executeNextTaskitem() {
        if (taskItemChain != null) {
            int currentTaskitemIndex = taskItemChain.getTaskItems().indexOf(this);
            int nextTaskitemIndex = currentTaskitemIndex + 1;
            if (currentTaskitemIndex + 1 < taskItemChain.getTaskItems().size()) {
                TaskItem taskItem = taskItemChain.getTaskItems().get(nextTaskitemIndex);
                TaskService taskService = SpringContextUtils.getBean(TaskService.class);
                if (taskService == null) {
                    taskService = new TaskServiceImpl();
                }
                taskService.asyncExecuteTask(taskItem);
            } else {
                System.out.println("No next task item" + this);
            }
        }
    }

    /**
     *  等待任务完成/结束
     *
     * @throws InterruptedException
     */
    public void waitForComplete() throws InterruptedException {
        while (this.consumerCount != this.consumerCompletedCount) {
            if (STOPPED_STATUS.contains(getStatus())) {
                break;
            }
            Thread.sleep(2000);
        }
    }
}
