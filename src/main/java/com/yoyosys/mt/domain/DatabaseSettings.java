package com.yoyosys.mt.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by simon on 2019/2/28.
 */

@Data
public class DatabaseSettings implements Serializable {
    private String id;
    private String name;
    private String jdbcUrl;
    private String username;
    private String password;
    private String sql;
    private Date createTime;
    private Date updateTime;
}
