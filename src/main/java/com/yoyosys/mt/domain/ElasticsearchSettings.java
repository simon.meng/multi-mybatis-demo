package com.yoyosys.mt.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by simon on 2019/2/28.
 */
@Data
public class ElasticsearchSettings implements Serializable {

    private String id;
    private String name;
    private String url;
    private String indexName;
    private String indexMapping;
    private String idKey;
    private String docType = "_doc";
    private int retryCount = 2;
    private Date createTime;
    private Date updateTime;
}
