package com.yoyosys.mt.service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class WaitTest {

    private Lock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        final WaitTest test = new WaitTest();
        Thread t = new Thread(() -> {
            test.testWait();
        });
        t.start();
        Thread.sleep(1000);
        synchronized (test) {
            System.out.println("sss");
            test.notify();
        }
    }

    public synchronized void testWait() {
        System.out.println("Start-----");
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("End-------");
    }
}