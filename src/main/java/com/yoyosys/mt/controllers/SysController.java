package com.yoyosys.mt.controllers;

//import com.github.pagehelper.Page;
//import com.github.pagehelper.PageHelper;

import com.vip.vjtools.vjkit.mapper.XmlMapper;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.mapper.primary.AddressMapper;
import com.yoyosys.mt.rule.Rule;
import com.yoyosys.mt.web.JsonObject;
import com.yoyosys.mt.web.WebHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@Api(description = "System controller for manage system")
@RequestMapping("/sys")
public class SysController {

    @Autowired
    AddressMapper addressMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    @Qualifier("systemJdbcTemplate")
    JdbcTemplate systemJdbcTemplate;


    @GetMapping(value = "/rule/{id}", produces = "application/json")
    @ApiOperation("Get Rule")
    @ResponseBody
    public JsonObject rule(@PathVariable String id)throws IOException {
        return WebHelper.toJsonObject(IOUtils.resourceToString("rule.xml", Charset.defaultCharset(),SysController.class.getClassLoader()));
    }

    @PostMapping(value = "/check", produces = "application/json")
    @ApiOperation("Check ")
    @ResponseBody

    public JsonObject check(@RequestBody Map<String,String> config) {
        Rule rule = XmlMapper.fromXml(config.get("xml"), Rule.class);
        String addr = config.get("addr");
        List<String> result = new ArrayList<>();
        if (addr != null) {
            for (String s : addr.split("\n")) {
                result.add(rule.normalize(s));
            }
        }

        return WebHelper.toJsonObject(rule.normalize(StringUtils.join(result, "\n")));
    }


    //#########################################################################################
    @GetMapping(value = "/print/{id}", produces = "application/json")
    @ApiOperation("Print data source info")
    public JsonObject print(@PathVariable String id, @RequestParam Map<String, Object> paras) throws SQLException {
        try (Connection conn = jdbcTemplate.getDataSource().getConnection()) {
            ResultSet columns = conn.getMetaData().getColumns(null, null, "User", null);
            while (columns.next()) {
                String columnName = columns.getString("COLUMN_NAME");
                String datatype = columns.getString("DATA_TYPE");
                String columnsize = columns.getString("COLUMN_SIZE");
                String decimaldigits = columns.getString("DECIMAL_DIGITS");
                String isNullable = columns.getString("IS_NULLABLE");
                String is_autoIncrment = columns.getString("IS_AUTOINCREMENT");
                //Printing results
                System.out.println(columnName + "---" + datatype + "---" + columnsize + "---" + decimaldigits + "---" + isNullable + "---" + is_autoIncrment);
            }
        }

//        redisTemplate.opsForSet().add("setsamples", new Date().getTime() + "");
//        Set o = redisTemplate.opsForSet().members("setsamples");
//        System.out.println(o);
//        Page<User> page = PageHelper.startPage(1, 10)
//                .doSelectPage(() -> userMapper.getUser(id));

        return WebHelper.successMessage();
    }
}
