package com.yoyosys.mt.controllers;

import com.yoyosys.mt.domain.ElasticsearchSettings;
import com.yoyosys.mt.manager.ElasticsearchSettingsManager;
import com.yoyosys.mt.utils.UUIDGenerator;
import com.yoyosys.mt.web.JsonObject;
import com.yoyosys.mt.web.WebHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 2019/2/28.
 */
@RestController
@RequestMapping()
public class ElasticSearchSettingsController {


    @Autowired
    private ElasticsearchSettingsManager elasticsearchSettingsManager;

    @PostMapping(value = "/es-setting", produces = "application/json")
    @ApiOperation(value = "Create elasticsearch settings")
    @ResponseBody
    public JsonObject create(@RequestBody ElasticsearchSettings settings) {
        settings.setId(UUIDGenerator.random(0));
        settings.setCreateTime(new Date());
        settings.setUpdateTime(new Date());
        elasticsearchSettingsManager.create(settings);
        return WebHelper.successMessage();
    }


    @PutMapping(value = "/es-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Update elasticsearch setting")
    @ResponseBody
    public JsonObject update(@PathVariable String id, @RequestBody ElasticsearchSettings settings) {
        int result = elasticsearchSettingsManager.update(settings);
        if (result == 0) {
            return WebHelper.errorMessage("settings 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }

    @DeleteMapping(value = "/es-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Delete es setting by id")
    @ResponseBody
    public JsonObject delete(@PathVariable String id) {
        if (elasticsearchSettingsManager.delete(id) == 0) {
            return WebHelper.errorMessage("ES setting 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }


    @PatchMapping(value = "/es-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Patch update elasticsearch setting")
    @ResponseBody
    public JsonObject patchUpdate(@PathVariable String id, @RequestBody ElasticsearchSettings settings) {
        settings.setId(id);
        settings.setUpdateTime(new Date());
        int result = elasticsearchSettingsManager.update(settings);
        if (result == 0) {
            return WebHelper.errorMessage("settings 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }


    @GetMapping(value = "/es-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Get es setting by id")
    @ResponseBody
    public JsonObject get(@PathVariable String id) {
        ElasticsearchSettings settings = elasticsearchSettingsManager.get(id);
        if (settings == null) {
            return WebHelper.errorMessage("settings 不存在! id = " + id);
        }
        return WebHelper.toJsonObject(settings);
    }


    @GetMapping(value = "/es-settings", produces = "application/json")
    @ApiOperation(value = "Query es settings")
    @ResponseBody
    public JsonObject query(@RequestParam Map<String, String> paras) {
        List<ElasticsearchSettings> settingsList = elasticsearchSettingsManager.findAll();
        return WebHelper.toJsonObject(settingsList);
    }
}
