package com.yoyosys.mt.controllers;

import com.yoyosys.mt.domain.TaskHistory;
import com.yoyosys.mt.manager.TaskHistoryManager;
import com.yoyosys.mt.utils.UUIDGenerator;
import com.yoyosys.mt.web.JsonObject;
import com.yoyosys.mt.web.WebHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by simon on 2019/2/28.
 */
@RestController
@RequestMapping()
public class TaskHistoryController {

    @Autowired
    private TaskHistoryManager taskHistoryManager;

    @PostMapping(value = "/task-history", produces = "application/json")
    @ApiOperation(value = "Create task-history")
    @ResponseBody
    public JsonObject create(@RequestBody TaskHistory taskHistory) {
        taskHistory.setId(UUIDGenerator.generateRandomTableId());
        taskHistoryManager.create(taskHistory);
        return WebHelper.successMessage();
    }


    @PutMapping(value = "/task-history/{id}", produces = "application/json")
    @ApiOperation(value = "Update task-history")
    @ResponseBody
    public JsonObject update(@PathVariable String id, @RequestBody TaskHistory taskHistory) {
        taskHistoryManager.update(taskHistory);
        return WebHelper.successMessage();
    }

    @PatchMapping(value = "/task-history/{id}", produces = "application/json")
    @ApiOperation(value = "Update task-history")
    @ResponseBody
    public JsonObject patch(@PathVariable String id, @RequestBody TaskHistory taskHistory) {
        taskHistoryManager.patch(taskHistory);
        return WebHelper.successMessage();
    }

    @GetMapping(value = "/task-history/{id}", produces = "application/json")
    @ApiOperation(value = "Get task-history by id")
    @ResponseBody
    public JsonObject get(@PathVariable String id) {
        return WebHelper.toJsonObject(taskHistoryManager.get(id));
    }


    @GetMapping(value = "/task-histories", produces = "application/json")
    @ApiOperation(value = "Query task-histories")
    @ResponseBody
    public JsonObject query(@RequestParam(required = false) Map<String, String> paras) {
        return WebHelper.toJsonObject(taskHistoryManager.findAll());
    }
}
