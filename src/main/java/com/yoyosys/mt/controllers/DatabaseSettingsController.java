package com.yoyosys.mt.controllers;

import com.yoyosys.mt.domain.DatabaseSettings;
import com.yoyosys.mt.manager.DatabaseSettingsManager;
import com.yoyosys.mt.utils.UUIDGenerator;
import com.yoyosys.mt.web.JsonObject;
import com.yoyosys.mt.web.WebHelper;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 2019/2/28.
 */
@RestController
@RequestMapping()
public class DatabaseSettingsController {


    @Autowired
    private DatabaseSettingsManager databaseSettingsManager;

    @PostMapping(value = "/db-setting", produces = "application/json")
    @ApiOperation(value = "Create db settings")
    @ResponseBody
    public JsonObject create(@RequestBody DatabaseSettings settings) {
        settings.setId(UUIDGenerator.random(0));
        settings.setCreateTime(new Date());
        settings.setUpdateTime(new Date());
        databaseSettingsManager.create(settings);
        return WebHelper.successMessage();
    }


    @PutMapping(value = "/db-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Update db setting setting")
    @ResponseBody
    public JsonObject update(@PathVariable String id, @RequestBody DatabaseSettings settings) {
        int result = databaseSettingsManager.update(settings);
        if (result == 0) {
            return WebHelper.errorMessage("settings 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }

    @PatchMapping(value = "/db-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Update db setting setting")
    @ResponseBody
    public JsonObject patch(@PathVariable String id, @RequestBody DatabaseSettings settings) {
        int result = databaseSettingsManager.patch(settings);
        if (result == 0) {
            return WebHelper.errorMessage("settings 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }

    @DeleteMapping(value = "/db-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Update db setting setting")
    @ResponseBody
    public JsonObject patch(@PathVariable String id) {
        databaseSettingsManager.delete(id);
        return WebHelper.successMessage();
    }

    @GetMapping(value = "/db-setting/{id}", produces = "application/json")
    @ApiOperation(value = "Get db setting by id")
    @ResponseBody
    public JsonObject get(@PathVariable String id) {
        DatabaseSettings settings = databaseSettingsManager.get(id);
        if (settings == null) {
            return WebHelper.errorMessage("settings 不存在! id = " + id);
        }
        return WebHelper.toJsonObject(settings);
    }



    @GetMapping(value = "/db-settings", produces = "application/json")
    @ApiOperation(value = "Query db settings")
    @ResponseBody
    public JsonObject query(@RequestParam Map<String, String> paras) {
        List<DatabaseSettings> settingsList = databaseSettingsManager.findAll();
        return WebHelper.toJsonObject(settingsList);
    }
}
