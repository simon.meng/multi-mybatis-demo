package com.yoyosys.mt.controllers;

import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.manager.TaskItemManager;
import com.yoyosys.mt.task.TaskService;
import com.yoyosys.mt.utils.UUIDGenerator;
import com.yoyosys.mt.web.JsonObject;
import com.yoyosys.mt.web.WebHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 2019/2/28.
 */
@RestController
@RequestMapping()
public class TaskItemController {

    @Autowired
    private TaskItemManager taskItemManager;

    @Autowired
    private TaskService taskService;

    @PostMapping(value = "/task-item", produces = "application/json")
    @ApiOperation(value = "Create new task item")
    @ResponseBody
    public JsonObject create(@RequestBody TaskItem taskItem) {
        taskItem.setId(UUIDGenerator.random(3));
        taskItem.setCreateTime(new Date());
        taskItemManager.create(taskItem);
        return WebHelper.successMessage(taskItem.getId());
    }

    @PutMapping(value = "/task-item/{id}", produces = "application/json")
    @ApiOperation(value = "Update task item")
    @ResponseBody
    public JsonObject update(@PathVariable String id, @RequestBody TaskItem taskItem) {
        taskItem.setUpdateTime(new Date());
        int result = taskItemManager.patch(taskItem);
        if (result == 0) {
            return WebHelper.errorMessage("task item 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }

    @PatchMapping(value = "/task-item/{id}", produces = "application/json")
    @ApiOperation(value = "Patch task item")
    @ResponseBody
    public JsonObject patch(@PathVariable String id, @RequestBody TaskItem taskItem) {
        taskItem.setUpdateTime(new Date());
        int result = taskItemManager.patch(taskItem);
        if (result == 0) {
            return WebHelper.errorMessage("task item 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }


    @DeleteMapping(value = "/task-item/{id}", produces = "application/json")
    @ApiOperation(value = "Delete task item by id")
    @ResponseBody
    public JsonObject delete(@PathVariable String id) {
        if (taskItemManager.delete(id) == 0) {
            return WebHelper.errorMessage("task item 不存在! id = " + id);
        }
        return WebHelper.successMessage();
    }


    @GetMapping(value = "/task-item/{id}", produces = "application/json")
    @ApiOperation(value = "Get task item by id")
    @ResponseBody
    public JsonObject get(@PathVariable String id) {
        TaskItem taskItem = taskItemManager.get(id);
        if (taskItem == null) {
            return WebHelper.errorMessage("task item 不存在! id = " + id);
        }
        return WebHelper.toJsonObject(taskItem);
    }


    @GetMapping(value = "/task-item/{id}/execute", produces = "application/json")
    @ApiOperation(value = "Execute task item")
    @ResponseBody
    public JsonObject start(@PathVariable String id) {
        TaskItem taskItem = taskItemManager.get(id);
        if (taskItem == null) {
            return WebHelper.errorMessage("taskitem 不存在,id = " + id);
        }
        taskService.executeTask(taskItem);
        return WebHelper.successMessage("成功执行taskitem, id = " + id);
    }

    @GetMapping(value = "/task-item/{id}/duplicate", produces = "application/json")
    @ApiOperation(value = "Duplicate/Copy task item")
    @ResponseBody
    public JsonObject duplicate(@PathVariable String id) {
        TaskItem taskItem = taskItemManager.get(id);
        if (taskItem == null) {
            return WebHelper.errorMessage("taskitem 不存在,id = " + id);
        }

        taskItem.setId(UUIDGenerator.random(3));
        taskItem.setCreateTime(new Date());
        taskItem.setUpdateTime(new Date());
        taskItemManager.create(taskItem);
        return WebHelper.successMessage("成功执行taskitem, id = " + id);
    }

    //------------------------------------------------
    // MULTIPLE ITEMS OPERATION
    //------------------------------------------------
    @GetMapping(value = "/task-items", produces = "application/json")
    @ApiOperation(value = "Query task items")
    @ResponseBody
    public JsonObject query(
            @RequestParam(required = false) Map<String, String> paras, HttpServletRequest request) {
        return WebHelper.toJsonObject(taskItemManager.pageQuery(paras));
    }

    @PostMapping(value = "/task-items/delete", produces = "application/json")
    @ApiOperation(value = "Delete multiple task item")
    @ResponseBody
    public JsonObject deleteMultipleTaskItems(@RequestBody List<String> ids) {
        taskItemManager.deleteMultipleByPrimaryKey(ids);
        return WebHelper.successMessage();
    }
}
