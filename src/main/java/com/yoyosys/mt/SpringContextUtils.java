package com.yoyosys.mt;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by simon on 2019/3/1.
 */
@Component
public class SpringContextUtils implements ApplicationContextAware {

    private static ApplicationContext ctx;

    public static <T> T getBean(Class<T> t) {
        if (ctx != null) {
            return ctx.getBean(t);
        }
        return null;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }
}
