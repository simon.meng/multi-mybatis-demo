package com.yoyosys.mt.manager;

import com.yoyosys.mt.domain.DatabaseSettings;
import com.yoyosys.mt.mapper.system.DatabaseSettingsMapper;
import org.springframework.stereotype.Component;

/**
 * Created by simon on 2019/2/28.
 */
@Component
public class DatabaseSettingsManagerImpl extends AbstractCommonManager<DatabaseSettingsMapper, DatabaseSettings, String> implements DatabaseSettingsManager {


}
