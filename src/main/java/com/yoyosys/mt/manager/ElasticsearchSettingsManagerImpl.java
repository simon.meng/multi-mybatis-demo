package com.yoyosys.mt.manager;

import com.yoyosys.mt.domain.ElasticsearchSettings;
import com.yoyosys.mt.mapper.system.ElasticsearchSettingsMapper;
import org.springframework.stereotype.Component;

/**
 * Created by simon on 2019/2/28.
 */
@Component
public class ElasticsearchSettingsManagerImpl extends AbstractCommonManager<ElasticsearchSettingsMapper, ElasticsearchSettings, String> implements ElasticsearchSettingsManager {
}
