package com.yoyosys.mt.manager;

import com.github.pagehelper.PageInfo;
import com.yoyosys.mt.domain.TaskItem;

import java.util.Map;

/**
 * Created by simon on 2019/2/27.
 */
public interface TaskItemManager extends CommonManager<TaskItem, String> {

    PageInfo<TaskItem> pageQuery(Map<String,String> paras);
}
