package com.yoyosys.mt.manager;

import com.yoyosys.mt.domain.TaskHistory;
import com.yoyosys.mt.mapper.system.TaskHistoryMapper;
import org.springframework.stereotype.Service;

/**
 * Created by simon on 2019/2/28.
 */
@Service
public class TaskHistoryManagerImpl extends AbstractCommonManager<TaskHistoryMapper, TaskHistory, String> implements TaskHistoryManager {
}


