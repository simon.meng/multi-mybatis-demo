package com.yoyosys.mt.manager;

import com.yoyosys.mt.domain.ElasticsearchSettings;

/**
 * Created by simon on 2019/2/28.
 */
public interface ElasticsearchSettingsManager extends CommonManager<ElasticsearchSettings, String> {
}
