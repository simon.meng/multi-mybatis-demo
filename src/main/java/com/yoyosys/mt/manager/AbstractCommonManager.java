package com.yoyosys.mt.manager;

import com.yoyosys.mt.mapper.CrudMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by simon on 2019/2/28.
 */
public abstract class AbstractCommonManager<E extends CrudMapper<T, ID>, T, ID> implements CommonManager<T, ID> {

    @Autowired
    private E mappger;

    @Override
    public T get(ID id) {
        return mappger.selectByPrimaryKey(id);
    }

    @Override
    public int create(T t) {
        return mappger.insert(t);
    }

    @Override
    public int update(T t) {
        return mappger.updateByPrimaryKey(t);
    }

    @Override
    public int patch(T obj) {
        return mappger.updateByPrimaryKeySelective(obj);
    }

    @Override
    public int delete(ID id) {
        return mappger.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteMultipleByPrimaryKey(List<ID> ids) {
        if (ids != null && !ids.isEmpty()) {
            return mappger.deleteMultipleByPrimaryKey(ids);
        }
        return 0;
    }

    @Override
    public List<T> findAll() {
        return mappger.findAll();
    }

    public E getMappger() {
        return mappger;
    }

    public void setMappger(E mappger) {
        this.mappger = mappger;
    }
}
