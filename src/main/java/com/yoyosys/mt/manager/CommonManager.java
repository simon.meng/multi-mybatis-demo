package com.yoyosys.mt.manager;

import java.util.List;

/**
 * Created by simon on 2019/2/28.
 */
public interface CommonManager<T, ID> {

    T get(ID id);

    int create(T obj);

    int update(T obj);

    int patch(T obj);

    int delete(ID id);

    int deleteMultipleByPrimaryKey(List<ID> ids);

    List<T> findAll();

}
