package com.yoyosys.mt.manager;

import com.yoyosys.mt.domain.TaskHistory;
import org.springframework.stereotype.Service;

/**
 * Created by simon on 2019/2/28.
 */
@Service
public interface TaskHistoryManager extends CommonManager<TaskHistory, String> {
}
