package com.yoyosys.mt.manager;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.mapper.system.TaskItemMapper;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by simon on 2019/2/27.
 */
@Component
public class TaskItemManagerImpl extends AbstractCommonManager<TaskItemMapper, TaskItem, String> implements TaskItemManager {

    @Override
    public PageInfo<TaskItem> pageQuery(Map<String,String> paras) {

        int pageNum = Integer.parseInt(paras.getOrDefault("pageNum", "1").trim());
        int pageSize = Integer.parseInt(paras.getOrDefault("pageSize", "10").trim());


        PageInfo<TaskItem> pageInfo = PageHelper.startPage(pageNum, pageSize)
                .doSelectPageInfo(() -> getMappger().findAll());

        return pageInfo;
    }
}
