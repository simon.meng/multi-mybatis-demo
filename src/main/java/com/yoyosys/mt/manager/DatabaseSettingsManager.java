package com.yoyosys.mt.manager;

import com.yoyosys.mt.domain.DatabaseSettings;

/**
 * Created by simon on 2019/2/28.
 */
public interface DatabaseSettingsManager extends CommonManager<DatabaseSettings, String> {
}
