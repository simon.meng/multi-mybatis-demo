package com.yoyosys.mt.utils;

import com.yoyosys.mt.SpringContextUtils;
import com.yoyosys.mt.domain.TaskHistory;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.manager.TaskHistoryManager;

import java.util.Date;

public class TaskHistoryHelper {

    public static void addTaskHistory(TaskItem taskItem, String status, String description) {
        TaskHistoryManager manager = SpringContextUtils.getBean(TaskHistoryManager.class);
        if (manager != null) {
            TaskHistory taskHistory = new TaskHistory();
            taskHistory.setId(UUIDGenerator.generateRandomTableId());
            taskHistory.setAuditTime(new Date());
            taskHistory.setExecutionId(taskItem.getTaskContext().get("EXECUTION_ID").toString());
            taskHistory.setTaskitemId(taskItem.getId());
            taskHistory.setDescription(description);
            taskHistory.setStatus(status);
            manager.create(taskHistory);
        }
    }

    public static void addTaskHistory(String instanceId, String taskitemId, String status, String description, Date auditTime) {
        TaskHistoryManager manager = SpringContextUtils.getBean(TaskHistoryManager.class);
        if (manager != null) {
            TaskHistory taskHistory = new TaskHistory();
            taskHistory.setId(instanceId);
            taskHistory.setAuditTime(auditTime);
            taskHistory.setTaskitemId(taskitemId);
            taskHistory.setDescription(description);
            taskHistory.setStatus(status);
            manager.create(taskHistory);
        }
    }
}
