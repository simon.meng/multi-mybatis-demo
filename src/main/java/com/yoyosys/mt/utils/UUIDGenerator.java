package com.yoyosys.mt.utils;

import com.vip.vjtools.vjkit.time.DateFormatUtil;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;

/**
 * Created by simon on 2019/3/5.
 */
public class UUIDGenerator {

    public static String generateRandomTableId(){
        return random(4);
    }

    public static String random(int length) {
        return DateFormatUtil.formatDate("yyyyMMddHHmmssSSS", new Date()) + RandomStringUtils.randomNumeric(length);
    }
}
