package com.yoyosys.mt.task.event;

/**
 * Created by simon on 2019/3/2.
 */
public class TaskTerminatedEvent extends TaskEvent {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public TaskTerminatedEvent(Object source) {
        super(source);
    }
}
