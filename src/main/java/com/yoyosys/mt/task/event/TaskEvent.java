package com.yoyosys.mt.task.event;

import java.util.EventObject;

/**
 * Created by simon on 2019/3/2.
 */
public abstract class TaskEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public TaskEvent(Object source) {
        super(source);
    }
}
