package com.yoyosys.mt.task;


import com.yoyosys.mt.domain.TaskItem;

import java.util.List;

/**
 * Created by simon on 2019/2/26.
 */
public interface TaskService {

    /**
     * 执行任务，直到任务结束为止(所有生产者，消费者都结束)
     *
     * @param taskItem
     */
    int executeTask(TaskItem taskItem);

    /**
     * 执行多个任务。由于单个任务是同步阻塞的，所以各个任务是依次顺序执行的。
     *
     * @param taskItems
     */
    void executeTasks(List<TaskItem> taskItems);


    /**
     * 异步执行任务
     *
     * @param taskItem
     */
    void asyncExecuteTask(TaskItem taskItem);

}
