package com.yoyosys.mt.task;

import com.yoyosys.mt.domain.TaskItem;

import java.util.ArrayList;
import java.util.List;

public class TaskItemChain {

    private List<TaskItem> taskItems = new ArrayList<>();

    public void addTaskitem(TaskItem taskItem) {
        taskItem.setTaskItemChain(this);
        taskItems.add(taskItem);
    }

    public List<TaskItem> getTaskItems() {
        return taskItems;
    }

    public void setTaskItems(List<TaskItem> taskItems) {
        this.taskItems = taskItems;
    }

}