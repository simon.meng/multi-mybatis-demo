package com.yoyosys.mt.task;

import com.yoyosys.mt.task.event.TaskEvent;

/**
 * Created by simon on 2019/2/28.
 */
public interface TaskEventListener {
    void onEvent(TaskEvent taskEvent);
}
