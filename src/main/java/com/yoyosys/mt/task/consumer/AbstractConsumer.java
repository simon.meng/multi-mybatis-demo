package com.yoyosys.mt.task.consumer;

import com.yoyosys.mt.domain.TaskHistory;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.task.TaskItemStatus;
import com.yoyosys.mt.utils.TaskHistoryHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by simon on 2019/2/26.
 */

public abstract class AbstractConsumer<T> implements Consumer {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private transient TaskItem<T> taskItem;

    public AbstractConsumer(TaskItem<T> taskItem) {
        this.taskItem = taskItem;
    }

    /**
     * 默认实现Runnable接口
     */
    public void run() {

        TaskHistoryHelper.addTaskHistory(getTaskItem(),"consumer begin..." , "开始消费");

        //还在生产，还有产品？ 那就消费吧。
        while (taskItem.getStatus() == TaskItemStatus.RUNNING || !taskItem.getQueue().isEmpty()) {
            try {
                //等了1秒都没产品，去问下什么情况吧(还在生产，还有产品？).....
                T obj = taskItem.getQueue().poll(1, TimeUnit.SECONDS);
                if (obj != null) {
                    try {
                        consume(obj);
                    } catch (Exception e) {
                        //TODO: 当消费异常时, 该怎么办？(可进生其它复杂的控制处理)
                        logger.info(e.getMessage(), e);
                    }
                }
            } catch (InterruptedException e) {
                logger.info(e.getMessage(), e);
            }
        }

        //消费完成, 记分
        taskItem.increaseConsumerCompletedCount();

        //打完, 收工
        this.onCompleted();
    }

    /**
     * 继承AbstractConsumer后，必须实现consume方法, 处理对像
     *
     * @param obj
     */
    protected abstract void consume(T obj);

    /**
     * 实现此方法，可用来关闭资源
     */
    protected abstract void onCompleted();


    public TaskItem getTaskItem() {
        return taskItem;
    }
}
