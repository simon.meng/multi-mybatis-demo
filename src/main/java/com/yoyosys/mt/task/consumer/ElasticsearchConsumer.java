package com.yoyosys.mt.task.consumer;

import com.yoyosys.mt.SpringContextUtils;
import com.yoyosys.mt.domain.ElasticsearchSettings;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.manager.ElasticsearchSettingsManager;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 2019/2/26.
 */
public class ElasticsearchConsumer extends AbstractConsumer<List<Map<String, Object>>> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private ElasticsearchSettings settings;

    private RestHighLevelClient client;


    public ElasticsearchConsumer(TaskItem taskItem) {

        super(taskItem);

        if (taskItem.getConsumerSettings() == null) {
            ElasticsearchSettingsManager mgr = SpringContextUtils.getBean(ElasticsearchSettingsManager.class);
            this.settings = mgr.get(getTaskItem().getConsumerId());
        } else {
            this.settings = (ElasticsearchSettings) taskItem.getConsumerSettings();
        }
        if (taskItem.getConsumerEsIndexName() != null) {
            this.settings.setIndexName(taskItem.getConsumerEsIndexName());
        }
        String[] hosts = settings.getUrl().split(",");
        HttpHost[] httpHosts = new HttpHost[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            String[] hostPorts = hosts[i].split(":");
            httpHosts[i] = new HttpHost(hostPorts[0], Integer.valueOf(hostPorts[1]));
        }
        this.client = new RestHighLevelClient(RestClient.builder(httpHosts));
    }

    /**
     * 继承AbstractConsumer后，必须实现consume方法, 处理对像
     *
     * @param obj
     */
    @Override
    public void consume(List<Map<String, Object>> obj) {
        createDocuments(obj, settings.getRetryCount());
    }

    public void createDocuments(Object obj, int retry) {
        //XContentBuilderExtension

        if (retry > 0) {
            BulkRequest request = new BulkRequest();
            String idKey = getTaskItem().getConsumerEsIdKey();
            for (Map<String, Object> record : (List<Map<String, Object>>) obj) {
                request.add(new IndexRequest(settings.getIndexName(), settings.getDocType(), (String) record.get(idKey)).source(record));
            }
            try {
                BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
                //System.out.println(this + " --> " + bulkResponse.getItems());
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
                createDocuments(obj, --retry);
            }
        } else {

        }
    }

    /**
     * 实现此方法，可用来关闭资源
     */
    @Override
    public void onCompleted() {
        IOUtils.closeQuietly(client);
    }
}
