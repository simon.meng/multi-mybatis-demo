package com.yoyosys.mt.task.exception;

/**
 * Created by simon on 2019/3/3.
 */
public class TaskTerminatedExecption extends Exception{
    public TaskTerminatedExecption() {
        super();
    }

    public TaskTerminatedExecption(String message) {
        super(message);
    }

    public TaskTerminatedExecption(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskTerminatedExecption(Throwable cause) {
        super(cause);
    }

    protected TaskTerminatedExecption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
