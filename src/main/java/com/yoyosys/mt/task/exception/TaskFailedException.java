package com.yoyosys.mt.task.exception;

/**
 * Created by simon on 2019/3/3.
 */
public class TaskFailedException extends Exception {
    public TaskFailedException() {
        super();
    }

    public TaskFailedException(String message) {
        super(message);
    }

    public TaskFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskFailedException(Throwable cause) {
        super(cause);
    }

    protected TaskFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
