package com.yoyosys.mt.task.exception;

/**
 * Created by simon on 2019/3/3.
 */
public class TaskConsumerNotFoundException extends Exception{
    public TaskConsumerNotFoundException() {
    }

    public TaskConsumerNotFoundException(String message) {
        super(message);
    }

    public TaskConsumerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskConsumerNotFoundException(Throwable cause) {
        super(cause);
    }

    public TaskConsumerNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
