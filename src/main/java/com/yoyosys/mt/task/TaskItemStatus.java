package com.yoyosys.mt.task;

import java.util.Arrays;
import java.util.List;

/**
 * Created by simon on 2019/3/3.
 */
public interface TaskItemStatus {

     int INIT = 0;             // 初始化(默认状态)
     int STARTTD = 100;        // 任务已实启动, （但不一定是running状态，可能在等待线程去执行)
     int RUNNING = 300;        // 可以进行生产/消费
     int PRODUCE_COMPLETED = 301;  //生产完成
     int CONSUMER_COMPLETED = 302; //消费完成
     int BAD_CONF   = 400;     // 配置有问题
     int TERMINATED = 499;     // 任务被手动结束
     int ERROR= 500;           // 内部错误
     int END = 200;            // 任务正常结束

     List<Integer> RUNNING_STATUS = Arrays.asList(RUNNING, PRODUCE_COMPLETED, CONSUMER_COMPLETED);
     List<Integer> STOPPED_STATUS = Arrays.asList(BAD_CONF, ERROR, END);
}

/*

任务状态转换表:

     /-->  BAD_CONF
     |
INIT --->  RUNNING --> PRODCE_COMPOLETED--> CONSUMER_COMPLETED  --->END
     |         |         /                     /
     \-->  TERMINATED<--/---------------------/


*/