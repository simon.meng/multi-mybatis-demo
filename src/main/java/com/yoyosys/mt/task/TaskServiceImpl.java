package com.yoyosys.mt.task;


import com.yoyosys.mt.domain.TaskHistory;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.manager.TaskHistoryManager;
import com.yoyosys.mt.manager.TaskItemManager;
import com.yoyosys.mt.task.consumer.Consumer;
import com.yoyosys.mt.task.producer.Producer;
import com.yoyosys.mt.utils.TaskHistoryHelper;
import com.yoyosys.mt.utils.UUIDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by simon on 2019/2/26.
 */
@Component
public class TaskServiceImpl implements TaskService {

    public static List<TaskItem> taskItems = new ArrayList<>();
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TaskItemManager taskItemManager;

    @Autowired
    private TaskHistoryManager taskHistoryManager;

    //生产者线程池。默认最多有4个任务在生产，新进来的任务会添加到队列
    //众所周知, 这个线程池的队列是无限大的(内存够大就行)。。。可参考TOMCAT兄的实现改进
    private ExecutorService producerExecuteService = Executors.newFixedThreadPool(4);

    //消费者线程池.
    private ExecutorService consumerExecuteService = Executors.newFixedThreadPool(20);

    /**
     * 执行任务，直到任务结束为止(所有生产者，消费者都结束)
     *
     * @param taskItem
     */
    @Override
    public int executeTask(TaskItem taskItem) {
        asyncExecuteTask(taskItem);
        try {
            //等到生产者，和所有消费者都完工为止
            taskItem.waitForComplete();
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
        return taskItem.getStatus();
    }

    /**
     * 执行多个任务。由于单个任务是同步阻塞的，所以各个任务是依次顺序执行的。
     * 前一个任务完后，执行一下任务
     *
     * @param taskItems
     */
    @Override
    public void executeTasks(List<TaskItem> taskItems) {
        for (TaskItem taskItem : taskItems) {
            //若执行的任务没有成功结束，则退出循环.
            if (executeTask(taskItem) != TaskItemStatus.END) {
                break;
            }
        }
    }

    /**
     * 异步执行任务
     *
     * @param taskItem
     */
    @Override
    public void asyncExecuteTask(TaskItem taskItem) {
        //重置
        taskItem.setProducerCompletedCount(0);
        taskItem.setConsumerCompletedCount(0);
        if (taskItem.getTaskContext() == null) {
            taskItem.setTaskContext(new HashMap<>());
        }
        //每次执行，生成一个ID
        taskItem.getTaskContext().put("EXECUTION_ID", UUIDGenerator.random(0));
        TaskHistoryHelper.addTaskHistory(taskItem, "task-begin", "任务开始");

        //通知开工
        taskItem.setStatus(TaskItemStatus.RUNNING);
        if (taskItemManager != null) {
            taskItemManager.patch(taskItem);
        }
        for (int i = 0; i < taskItem.getProducerCount(); i++) {
            Producer producer = TaskHelper.newProducer(taskItem);
            if (producer == null) {
                taskItem.setStatus(TaskItemStatus.BAD_CONF);
                throw new RuntimeException("invalid taskitem, unable to get consumer," + taskItem);
            }
            producerExecuteService.execute(producer);
        }

        //根据taskitem的配置,初始化消费者
        for (int i = 0; i < taskItem.getConsumerCount(); i++) {
            Consumer consumer = TaskHelper.newConsumer(taskItem);
            if (consumer == null) {
                //taskItem.setProducing(false);
                taskItem.setStatus(TaskItemStatus.BAD_CONF);
                throw new RuntimeException("invalid taskitem, unable to get consumer," + taskItem);
            }
            consumerExecuteService.execute(consumer);
        }
    }
}
