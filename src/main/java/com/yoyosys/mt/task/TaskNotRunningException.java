package com.yoyosys.mt.task;

/**
 * Created by simon on 2019/3/2.
 */
public class TaskNotRunningException extends Exception {
    public TaskNotRunningException() {
        super();
    }

    public TaskNotRunningException(String message) {
        super(message);
    }

    public TaskNotRunningException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskNotRunningException(Throwable cause) {
        super(cause);
    }

    protected TaskNotRunningException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
