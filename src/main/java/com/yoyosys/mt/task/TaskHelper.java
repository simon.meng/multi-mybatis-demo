package com.yoyosys.mt.task;

import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.task.consumer.Consumer;
import com.yoyosys.mt.task.producer.Producer;

import java.lang.reflect.Constructor;

/**
 * Created by simon on 2019/2/28.
 */
public class TaskHelper {

    /**
     * 根据taskitem的配置构造一个Consumer
     *
     * @param taskItem
     * @return null 如果taskitem 配置不正解
     */
    public static Consumer newConsumer(TaskItem taskItem) {
        try {
            Class myClass = Class.forName(taskItem.getConsumerType());
            Constructor constructor = myClass.getConstructor(TaskItem.class);
            return (Consumer) constructor.newInstance(taskItem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据taskitem的配置构造一个Producer
     *
     * @param taskItem
     * @return null 如果taskitem 配置不正解
     */
    public static Producer newProducer(TaskItem taskItem) {
        try {
            Class myClass = Class.forName(taskItem.getProducerType());
            Constructor constructor = myClass.getConstructor(TaskItem.class);
            return (Producer) constructor.newInstance(taskItem);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
