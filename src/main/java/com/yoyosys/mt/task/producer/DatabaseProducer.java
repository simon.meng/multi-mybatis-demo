package com.yoyosys.mt.task.producer;

import com.yoyosys.mt.SpringContextUtils;
import com.yoyosys.mt.domain.DatabaseSettings;
import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.manager.DatabaseSettingsManager;
import com.yoyosys.mt.task.TaskNotRunningException;
import com.yoyosys.mt.utils.TaskHistoryHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by simon on 2019/2/26.
 */
public class DatabaseProducer extends AbstractProducer<List<Map<String, Object>>> {

    private DatabaseSettings settings;

    private Logger logger = LoggerFactory.getLogger(getClass());

    public DatabaseProducer(TaskItem<List<Map<String, Object>>> taskItem) {
        super(taskItem);
        if (taskItem.getProducerSettings() == null) {
            DatabaseSettingsManager mgr = SpringContextUtils.getBean(DatabaseSettingsManager.class);
            this.settings = mgr.get(getTaskItem().getProducerId());
        } else {
            this.settings = (DatabaseSettings) taskItem.getProducerSettings();
        }
        if (taskItem.getProducerDbSql() != null) {
            this.settings.setSql(taskItem.getProducerDbSql());
        }
    }

    public int produce() {
        SingleConnectionDataSource dataSource = null;
        try {
            TaskHistoryHelper.addTaskHistory(getTaskItem(), Thread.currentThread().getName() + " - producer-begin" , "生产开始");
            dataSource = new SingleConnectionDataSource(settings.getJdbcUrl(), settings.getUsername(), settings.getPassword(), false);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            final AtomicReference<List<Map<String, Object>>> atomicReference = new AtomicReference<>(new ArrayList<>());
            final AtomicInteger atomicInteger = new AtomicInteger();
            final String sql = settings.getSql();
            jdbcTemplate.query(sql, resultSet -> {
                if (atomicInteger.incrementAndGet() % 10000 == 0) {
                    logger.info("put rows to queue, total size = {}", atomicInteger.intValue());
                }
                Map<String, Object> data = new HashMap<>();
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    if (resultSet.getObject(i) != null && resultSet.getObject(i) instanceof Timestamp) {
                        Timestamp timestamp = (Timestamp) resultSet.getObject(i);
                        data.put(resultSet.getMetaData().getColumnName(i), new Date(timestamp.getTime()));
                    } else {
                        data.put(resultSet.getMetaData().getColumnName(i), resultSet.getObject(i));
                    }
                }
                atomicReference.get().add(data);

                //查询数据库，每100条记录为一个批次提交到任务队列
                if (atomicReference.get().size() == 200) {
                    try {
                        this.addProduceToQueue(atomicReference.get());
                        atomicReference.set(new ArrayList<>());
                    } catch (InterruptedException e) {
                        logger.error(e.getMessage(), e);
                    } catch (TaskNotRunningException e) {
                        TaskHistoryHelper.addTaskHistory(getTaskItem(), "task-stopped", "任务已停止" + e.getMessage());
                        throw new RuntimeException(e.getMessage(), e);
                    }
                }
            });

            //提交不足100条记录的批次到任务队列
            if (!atomicReference.get().isEmpty()) {
                try {
                    this.addProduceToQueue(atomicReference.get());
                } catch (InterruptedException e) {
                    logger.error(e.getMessage(), e);
                } catch (TaskNotRunningException e) {
                    TaskHistoryHelper.addTaskHistory(getTaskItem(), "task-stopped", "任务已停止" + e.getMessage());
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
            TaskHistoryHelper.addTaskHistory(getTaskItem(), Thread.currentThread().getName() + " - producer-end" , "生产完成");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            TaskHistoryHelper.addTaskHistory(getTaskItem(), "producer-exception", "生产异常");
            return -1;
        } finally {
            dataSource.destroy();
        }
        return 0;
    }

    @Override
    protected void onCompleted() {

    }

    public DatabaseSettings getSettings() {
        return settings;
    }

    public void setSettings(DatabaseSettings settings) {
        this.settings = settings;
    }
}
