package com.yoyosys.mt.task.producer;

/**
 * Created by simon on 2019/3/3.
 */
public interface ProducerStatus {
    int INIT = 0;
    int WAITTING = 0;
    int STARTTD = 0;
    int TERMINATED = 0;
    int ERROR = 0;
    int END = 0;
}
