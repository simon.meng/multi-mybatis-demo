package com.yoyosys.mt.task.producer;

import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.task.TaskItemStatus;
import com.yoyosys.mt.task.TaskNotRunningException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by simon on 2019/2/27.
 */
public abstract class AbstractProducer<T> implements Producer {

    private TaskItem<T> taskItem;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractProducer(TaskItem<T> taskItem) {
        this.taskItem = taskItem;
    }

    /**
     * 默认实现Runnable接口
     */
    public void run() {
        logger.info("task item id={}, input_type={} producer start...", taskItem.getId(), taskItem.getProducerType());

        //生产
        int result;
        try {
            result = produce();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result = -1;
        }

        logger.info("Task producer completed, status = {} task item id={}, input_type={} producer done!", result, taskItem.getId(), taskItem.getProducerType());

        //收工
        onCompleted();

        //记分
        taskItem.increaseProducerCompletedCount();

    }

    /**
     * 子类实此方法, 用来生产产品，并源源不断将产品上架(addProduceToQueue(obj))
     */
    /**
     * @return 0:生产正常 -1: 生产有异常 1：生产被终结
     */
    protected abstract int produce();

    /**
     * 收工
     */
    protected abstract void onCompleted();

    /**
     * 产品上架(queue), 给人消费
     * 只有task是running 的状态，才可以将成品添加到队列。
     * @param obj
     * @throws InterruptedException
     * @throws TaskNotRunningException 如果Taskitem 不是运行状态
     */
    public void addProduceToQueue(T obj) throws InterruptedException, TaskNotRunningException {
        if(taskItem.getStatus() != TaskItemStatus.RUNNING) {
            throw new TaskNotRunningException("TASK is not running!, Can't put produce to queue");
        }

        //不停地将该产品上架,直到成功为止!
        while (!taskItem.getQueue().offer(obj, 1, TimeUnit.SECONDS)) {
            if(taskItem.getStatus() != TaskItemStatus.RUNNING) {
                throw new TaskNotRunningException("TASK is not running!, Can't put produce to queue");
            }
        }
    }

    public TaskItem getTaskItem() {
        return taskItem;
    }

}
