package com.yoyosys.mt.rule;


//import groovy.lang.Binding;
//import groovy.lang.GroovyShell;

import com.yoyosys.mt.utils.UUIDGenerator;
import org.joor.Reflect;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by simon on 2019/3/7.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Script implements Action {

    /**
     * The name of parameter passing to groovy environment/shell script
     * 可以使用该变量转换
     */
    @XmlAttribute
    private String key = "str";

    /**
     * groovy script
     */
    @XmlValue
    @XmlJavaTypeAdapter(AdapterCDATA.class)
    private String text;

    private static Map<String, Function> functions = new HashMap<>();


    public Script() {

    }

    public Script(String text) {
        this.text = text;
    }

    public synchronized Function getFunction(String text) {
        if (functions.containsKey(text)) {
            return functions.get(text);
        } else {
            String clazzName = "F" + UUIDGenerator.random(4);
            String clazzString = text.replaceFirst("class[\\s]+\\w+[\\s]+", "class " + clazzName + " ");
            Function<String, String> function = Reflect.compile(clazzName, clazzString).create().get();
            functions.put(text, function);
            return function;
        }
    }

    public String convert(String str) {
        if (text != null) {
            Function<String, String> function = getFunction(text);
            return function.apply(str);
        }
        return str;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
