package com.yoyosys.mt.rule;

public interface Action {
    String convert(String string);
}
