package com.yoyosys.mt.rule;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by simon on 2019/3/7.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Converter implements Action {

    @XmlTransient
    private static Map<String, Converter> converterMap = new HashMap();

    @XmlAttribute
    private String type;

    public Converter() {
    }

    public Converter(String type) {
        this.type = type;
    }

    public String convert(String string) {
        Converter converter = converterMap.get(type);
        if (converter != null) {
            return converter.convert(string);
        }
        // if converter not found
        return string;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
