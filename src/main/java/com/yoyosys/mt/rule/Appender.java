package com.yoyosys.mt.rule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by simon on 2019/3/7.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Appender implements Action {

    @XmlAttribute
    private String regex;

    @XmlAttribute
    private String text;

    public Appender() {
    }

    public Appender(String regex, String text) {
        this.regex = regex;
        this.text = text;
    }

    public String convert(String str) {
        if (regex != null && !regex.isEmpty()) {
            if (str.matches(regex)) {
                return str + text;
            }
        }
        return str;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
