package com.yoyosys.mt.rule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by simon on 2019/3/7.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Replacer implements Action {

    @XmlAttribute
    private String regex;

    @XmlAttribute
    private String replacement;

    public Replacer() {
    }

    public Replacer(String regex, String replacement) {
        this.regex = regex;
        this.replacement = replacement;
    }

    public String convert(String str) {
        if (regex != null && !regex.isEmpty()) {
            return str.replaceAll(regex, replacement);
        }
        // if regex not configured
        return str;
    }
}
