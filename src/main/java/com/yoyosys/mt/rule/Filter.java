package com.yoyosys.mt.rule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Created by simon on 2019/3/7.
 */
@XmlAccessorType(XmlAccessType.FIELD)

public class Filter implements Action {

    @XmlAttribute
    private String regex;

    public Filter() {

    }

    public Filter(String regex) {
        this.regex = regex;
    }

    public String convert(String str) {
        if (regex != null && !regex.isEmpty() && str.matches(regex)) {
            return null;
        }
        return str;
    }
}
