package com.yoyosys.mt.rule;

import com.vip.vjtools.vjkit.mapper.XmlMapper;
import lombok.Data;
import org.apache.commons.io.IOUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "rule")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Rule {

    @XmlElements({
            @XmlElement(name = "filter", type = Filter.class),
            @XmlElement(name = "replacer", type = Replacer.class),
            @XmlElement(name = "appender", type = Appender.class),
            @XmlElement(name = "script", type = Script.class),
            @XmlElement(name = "converter", type = Converter.class),
    })

    private List<Action> actions;

    public Rule() {
    }

    public String normalize(String str) {
        if (actions != null) {
            String tmp = str;
            for (Action action : actions) {
                if (tmp != null && !tmp.isEmpty()) {
                    tmp = action.convert(tmp);
                } else {
                    return tmp;
                }
            }
            return tmp;
        }
        return str;
    }
}
