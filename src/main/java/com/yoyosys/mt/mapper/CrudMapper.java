package com.yoyosys.mt.mapper;

import java.util.List;

/**
 * Created by simon on 2019/2/28.
 */
public interface CrudMapper<E, ID> {
    int insert(E e);

    int insertSelective(E e);

    int deleteByPrimaryKey(ID id);

    int deleteMultipleByPrimaryKey(List<ID> id);

    E selectByPrimaryKey(ID id);

    int updateByPrimaryKey(E e);

    int updateByPrimaryKeySelective(E e);

    List<E> findAll();
}
