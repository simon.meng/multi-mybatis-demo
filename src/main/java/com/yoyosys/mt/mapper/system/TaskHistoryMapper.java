package com.yoyosys.mt.mapper.system;

import com.yoyosys.mt.domain.TaskHistory;
import com.yoyosys.mt.mapper.CrudMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by simon on 2019/2/28.
 */
@Mapper
public interface TaskHistoryMapper extends CrudMapper<TaskHistory, String> {

}
