package com.yoyosys.mt.mapper.system;

import com.yoyosys.mt.domain.TaskItem;
import com.yoyosys.mt.mapper.CrudMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.cache.annotation.Cacheable;

/**
 * Created by simon on 2019/2/28.
 */
public interface TaskItemMapper extends CrudMapper<TaskItem, String> {

    @Cacheable("versions")
    @Select("SELECT DATABASE() || ' ' || H2VERSION() FROM DUAL")
    String getVersion();

//  @Select("Select id, name from User where id = #{id,jdbcType=VARCHAR}")
//	@Cacheable(value = "users", key = "#userId", unless = "#result.followers < 12000")
//	@Cacheable(value = "users", key = "#p0")
//  @Cacheable(value = "users")
//	@Cacheable(value = "users", key = "#id")
//	@CachePut
//    List<TaskItem> getTaskItem(String id);
//
//    @Insert("Insert INTO User (id, name) values (#{id}, #{name})")
//    int insert(Task user);

}
