package com.yoyosys.mt.mybatis;

import com.github.pagehelper.Page;
import com.github.pagehelper.dialect.AbstractHelperDialect;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;

import java.util.Map;

/**
 * @author Simon
 */
public class OracleDialect extends AbstractHelperDialect {

    public OracleDialect() {
        super();
    }

    @Override
    public Object processPageParameter(MappedStatement ms, Map<String, Object> paramMap, Page page, BoundSql boundSql, CacheKey pageKey) {
        paramMap.put(PAGEPARAMETER_FIRST, page.getEndRow());
        paramMap.put(PAGEPARAMETER_SECOND, page.getStartRow());
        //处理pageKey
        pageKey.update(page.getEndRow());
        pageKey.update(page.getStartRow());
        //处理参数配置
        handleParameter(boundSql, ms);
        return paramMap;
    }

    @Override
    public String getPageSql(String sql, Page page, CacheKey pageKey) {
        StringBuilder sqlBuilder = new StringBuilder(sql.length() + 120);
        sqlBuilder.append("SELECT * FROM ( ");
        sqlBuilder.append(" SELECT TMP_PAGE.*, ROWNUM __ROW_ID FROM ( ");
        sqlBuilder.append(sql);
        sqlBuilder.append(" ) WHERE __ROW_ID <= ? ) TMP_PAGE ");
        sqlBuilder.append("WHERE TMP_PAGE.__ROW_ID > ?");
        return sqlBuilder.toString();
    }

}
