package com.yoyosys.mt;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AnnotationAspect {
    private static final String POINTCUT_METHOD1 = "@annotation(com.yoyosys.webboot.client.MyAnnotation)";


    @Around(POINTCUT_METHOD1)
    public Object weaveJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            System.out.println("Beforeee " + joinPoint);
            return joinPoint.proceed();
        } finally {
            System.out.println("Afterrr " + joinPoint);
        }
    }
}
