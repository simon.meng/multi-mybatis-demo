
create table t_task_item
(
  id varchar(32) not null
    constraint t_task_item_pkey
      primary key,
  name varchar(100),
  status varchar(100),
  first_exe_time timestamp,
  last_exe_time timestamp,
  start_time timestamp,
  end_time timestamp,
  producer_id varchar(32),
  producer_type varchar(1000),
  producer_count integer,
  consumer_id varchar(32),
  consumer_type varchar(1000),
  consumer_count integer,
  create_time timestamp,
  update_time timestamp,
  producer_db_sql varchar(2000),
  consumer_es_index_name varchar(100),
  consumer_es_id_key varchar(100),
  producer_completed_count integer,
  consumer_completed_count integer,
  producer_status integer,
  consumer_status integer
);

alter table t_task_item owner to postgres;

create table t_db_settings
(
  id varchar(32) not null
    constraint db_settings_pkey
      primary key,
  name varchar(200),
  jdbc_url varchar(400),
  username varchar(100),
  password varchar(100),
  sql varchar(1000),
  create_time timestamp,
  update_time timestamp
);

alter table t_db_settings owner to postgres;

create table t_es_settings
(
  id varchar(32) not null
    constraint es_settings_pkey
      primary key,
  name varchar(200),
  url varchar(1000),
  index_name varchar(1000),
  doc_type varchar(100),
  retry_count integer,
  index_mapping varchar(2000),
  create_time timestamp,
  update_time timestamp,
  id_key varchar(32)
);

alter table t_es_settings owner to postgres;

create table t_task_history
(
  id varchar(32) not null
    constraint t_task_history_id_pk
      primary key,
  taskitem_id varchar(32),
  description varchar(1000),
  status varchar(200),
  audit_time timestamp,
  execution_id varchar(32)
);

alter table t_task_history owner to postgres;

