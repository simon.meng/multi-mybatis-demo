----
CREATE TABLE User (
  id   VARCHAR PRIMARY KEY,
  name VARCHAR
);

---
INSERT INTO User (id, name) VALUES ('0001', 'Simon');
INSERT INTO User (id, name) VALUES ('0002', 'Jack');
INSERT INTO User (id, name) VALUES ('0003', 'Summer');

---
CREATE TABLE t_db_settings
(
  id VARCHAR(32) PRIMARY KEY NOT NULL,
  name VARCHAR(200),
  jdbc_url VARCHAR(400),
  username VARCHAR(100),
  password VARCHAR(100),
  sql VARCHAR(1000)
);
CREATE TABLE t_es_settings
(
  id VARCHAR(32) PRIMARY KEY NOT NULL,
  name VARCHAR(200),
  url VARCHAR(1000),
  index_name VARCHAR(1000),
  doc_type VARCHAR(100),
  retry_count INTEGER,
  index_mapping VARCHAR(2000),
  create_time TIMESTAMP,
  update_time TIMESTAMP,
  id_key VARCHAR(32)
);
CREATE TABLE t_task_item
(
  id VARCHAR(32) PRIMARY KEY NOT NULL,
  name VARCHAR(100),
  status VARCHAR(100),
  first_exe_time TIMESTAMP,
  last_exe_time TIMESTAMP,
  start_time TIMESTAMP,
  end_time TIMESTAMP,
  producer_id VARCHAR(32),
  producer_type VARCHAR(1000),
  producer_count INTEGER,
--   producer_settings JSONB,
  consumer_id VARCHAR(32),
  consumer_type VARCHAR(1000),
  consumer_count INTEGER,
--   consumer_settings JSONB,
  create_time TIMESTAMP,
  update_time TIMESTAMP
);

--- test data
INSERT INTO public.t_db_settings (id, name, jdbc_url, username, password, sql)
VALUES ('1', 'address_all_data', 'jdbc:postgresql://localhost:32768/postgres', 'postgres', 'postgres', 'select * from address limit 20000');

INSERT INTO t_es_settings (id, name, url, index_name, doc_type, retry_count, index_mapping, create_time, update_time)
VALUES ('1', 'elasticsearch_localhost_9200', '127.0.0.1:9200', 'address_test', '_doc', 2, null, null, null);

---
INSERT INTO t_task_item (id, name, status, first_exe_time, last_exe_time, start_time, end_time, producer_id, producer_type, producer_count, consumer_id, consumer_type, consumer_count)
VALUES ('1', 'address-index', '301', NULL, '2019-03-04 15:33:29', '2019-03-01 22:31:59', '2019-03-01 22:32:07', '1','com.yoyosys.mt.task.producer.DatabaseProducer', 1, '1','com.yoyosys.mt.task.consumer.ElasticsearchConsumer', 4);